package com.paxos.rest;

public class MessageErrorResponse {
	
	private int status;
	private String err_msg;
	private long timeStamp;

	public MessageErrorResponse() {
		
	}
	
	public MessageErrorResponse(int status, String message, long timeStamp) {
		this.status = status;
		this.err_msg = message;
		this.timeStamp = timeStamp;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getMessage() {
		return err_msg;
	}

	public void setMessage(String message) {
		this.err_msg = message;
	}

	public long getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(long timeStamp) {
		this.timeStamp = timeStamp;
	}

}
