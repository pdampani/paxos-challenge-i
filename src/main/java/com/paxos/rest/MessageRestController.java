package com.paxos.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.paxos.entity.Digest;
import com.paxos.entity.Message;


@RestController
@RequestMapping("/api")
public class MessageRestController {

	private Map<String, String> messageMap = new HashMap<String, String>();
	
	
	@GetMapping("/messages")
	public List<Entry<String,String>> getMessages() {
		return new ArrayList<>(messageMap.entrySet());
	}
	
	@PostMapping("/messages")
	public Digest addMEssage(String message) {
			
		Message msg = new Message(message);
		Digest dgst = new Digest(message);
		messageMap.put(dgst.getDigest(), message);
		
		return dgst;
	}
	
	@GetMapping("/messages/{digest}")
	public Message getMessage(@PathVariable String digest) {
		
		if(messageMap.isEmpty() || !messageMap.containsKey(digest)) {
			throw new MessageNotFoundException("Message not found");
		}

		return new Message(messageMap.get(digest));
		
	}
	
	
	@ExceptionHandler
	public ResponseEntity<MessageErrorResponse> handleException(MessageNotFoundException exc) {
		
		MessageErrorResponse error = new MessageErrorResponse();
		
		error.setStatus(HttpStatus.NOT_FOUND.value());
		error.setMessage(exc.getMessage());
		error.setTimeStamp(System.currentTimeMillis());
		
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	 }

}
