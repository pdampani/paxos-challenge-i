package com.paxos.entity;

import org.apache.commons.codec.digest.DigestUtils;

public class Digest {

	private String digest;
	
	public Digest(String message) {
		this.digest = DigestUtils.sha256Hex(message);
	}

	public String getDigest() {
		return this.digest;
	}
	
	public void setDigest(String message) {
		this.digest = DigestUtils.sha256Hex(message);
	}
}
