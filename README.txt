How to use this, given that it's running on a server, in our case I've used Apache Tomcat/8.0.36
I've deployed this on Heroku, you can test it here https://intense-earth-30340.herokuapp.com/

pdampani> curl -X GET \
>   http://localhost:8080/api/messages
[]
pdampani> curl -X POST \
>   'http://localhost:8080/api/messages?message=foo'
{"digest":"2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae"}
pdampani> curl -X POST \
>   'http://localhost:8080/api/messages?message=fubar'
{"digest":"cc7b318a05d752cb1183d89545bc39017fdd811565520073fd17e323b738c283"}
pdampani> curl -X GET \
>   http://localhost:8080/api/messages/2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae
{"message":"foo"}
pdampani> curl -X GET   http://localhost:8080/api/messages/aaaaaaaaaaaaaaaaaaaaaaaa
{"status":404,"timeStamp":1543871444879,"message":"Message not found"}
pdampani> curl -X GET \
>   http://localhost:8080/api/messages
[{"2c26b46b68ffc68ff99b453c1d30413413422d706483bfa0f98a5e886266e7ae":"foo"},{"cc7b318a05d752cb1183d89545bc39017fdd811565520073fd17e323b738c283":"fubar"}]
